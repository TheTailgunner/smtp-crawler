#include <iostream>

#include <boost/asio.hpp>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <string>
#include <cstdlib>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <map>
#include <utility>

#include "worker.h"
#include "easylogging++.h"

#include <thread>

namespace po =  boost::program_options;

INITIALIZE_EASYLOGGINGPP

po::variables_map parse_command_line(int argc, char **argv) {
  po::options_description descr("smtp-crawler opts");
  descr.add_options()
    ("help", "help message")
    ("color", "enable color log")
    ("workers", po::value<std::size_t>(), "set working threads count")
    ("credentials", po::value<std::string>(), "file containing login info")
    ("hosts", po::value<std::string>(), "file containig host")
    ("stop-on-first", "enable terminating on first succesfully found login pair")
    ("log-level", po::value<std::string>(), "log level: error, debug, info" "[info]")
    ("output", po::value<std::string>(), "result file name")
    ("connection-timeout", po::value<std::size_t>(), "connection timeout, sec [default: 1]")
    ;

  po::variables_map opts;
  po::store(po::parse_command_line(argc, argv, descr), opts);
  po::notify(opts);

  if (opts.count("help")) {
    std::cout << descr << std::endl;
    std::exit(0);
  }

  if (!opts.count("credentials") ||
      !opts.count("hosts") ||
      !opts.count("output")) {
    throw std::runtime_error("not all input files provided");
  }

  return opts;
}

void configure_logger(const po::variables_map &_opts) {
  el::Configurations default_conf;
  default_conf.setToDefault();
  default_conf.set(el::Level::Info, 
            el::ConfigurationType::Format, "%datetime %level %msg");
  default_conf.set(el::Level::Error, 
            el::ConfigurationType::Format, "%datetime %loc %msg");
  default_conf.set(el::Level::Debug, 
            el::ConfigurationType::Format, "%datetime %func:%loc %msg");

  el::Loggers::addFlag(el::LoggingFlag::HierarchicalLogging);
  el::Loggers::setLoggingLevel(el::Level::Info);

  if (_opts.count("color"))
    el::Loggers::addFlag(el::LoggingFlag::ColoredTerminalOutput);

  if (_opts.count("log-level")) {
    el::Level log_level = el::LevelHelper::convertFromString(
						   _opts["log-level"].as<std::string>().c_str()
						   );
    if (log_level != el::Level::Unknown)
      el::Loggers::setLoggingLevel(log_level);
    else {
      std::cerr << "invalid log-level - fallback to default" << std::endl;
    }
  }

  el::Loggers::reconfigureLogger("default", default_conf);
  
  LOG(INFO) << "Logger configured";
}

void configure_worker(const po::variables_map &_opts,
		      crawler::worker &_w) {
  if (_opts.count("stop-on-first")) {
    _w.set_mode(crawler::worker::mode::stop_on_first);
  }

  if (_opts.count("workers"))
    _w.set_thread_count(
			 _opts["workers"].as<std::size_t>()
			 );
  if (_opts.count("connection-timeout"))
    _w.set_connect_timeout(
			_opts["connection-timeout"].as<std::size_t>()
			);
}

void read_and_add_credentials(po::variables_map &_opts,
			      crawler::worker &_w) {
  LOG(DEBUG) << "reading credentials";
  std::string creds_file = _opts["credentials"].as<std::string>();

  std::ifstream in;
  LOG(DEBUG) << "trying to open " << creds_file;
  in.open(creds_file);
  
  if (!in.good()) {
    std::string err_msg = creds_file + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(err_msg);
  }

  std::string line;
  std::size_t line_no = 0;

  using filter_map = std::map<std::pair<std::string, std::string>, int>;
  filter_map filter;

  while (std::getline(in, line)) {
    ++line_no;
    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, line,
			    boost::is_any_of(","));

    if (tokens.size() != 2) {
      LOG(WARNING) << creds_file << ":" << line_no
		   << " wrong fields count";
      continue;
    }

    // separating unique login-password pairs
    filter.insert({ std::make_pair(tokens[0], tokens[1]), 0});
  }

  for (filter_map::const_iterator itr = filter.cbegin();
       itr != filter.cend();
       ++itr) {
    _w.add_login_pair(itr->first.first, itr->first.second);
  }
  
  in.close();
}

void read_and_push_servers(po::variables_map &_opts,
			      crawler::worker &_w) {
  LOG(DEBUG) << "reading hosts";
  std::string hosts_file = _opts["hosts"].as<std::string>();

  std::ifstream in;
  LOG(DEBUG) << "trying to open " << hosts_file;
  in.open(hosts_file);
  
  if (!in.good()) {
    std::string err_msg = hosts_file + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(strerror(errno));
  }

  std::string line;
  std::size_t line_no{0};

  while (std::getline(in, line)) {
    ++line_no;
    std::vector<std::string> tokens;
    boost::algorithm::split(tokens, line,
			    boost::is_any_of(","));

    if (tokens.size() != 2) {
      LOG(WARNING) << hosts_file << ":" << line_no
		   << " wrong fields count";
      continue;
    }

    crawler::worker::task t;

    try {
      t.port = boost::lexical_cast<std::size_t>(tokens[1]);
    } catch (const boost::bad_lexical_cast &_exc) {
      LOG(WARNING) << hosts_file << ":" << line_no
		   << " wrong port value "
		   << _exc.what();
      continue;
    }

    t.ip = tokens[0];
    _w.push_task(t);
  }
  
  in.close();
}

void write_results(po::variables_map &_opts,
		   crawler::worker &_w) {
  std::string out_file = _opts["output"].as<std::string>();
  std::ofstream out;
  out.open(out_file);

  if (!out.good()) {
    std::string err_msg = out_file + ":" +
      std::string(strerror(errno));
    throw std::runtime_error(strerror(errno));
  }
  
  std::vector<crawler::worker::result_pair> results = _w.results();

  for (auto &_res : results) {
    out << _res.first.ip << ","
	<< _res.first.port << ","
	<< _res.second.first << ","
	<< _res.second.second << std::endl;
    out.flush();
  }

  out.close();
}

int main(int argc, char **argv) {
  boost::asio::io_service service;
  boost::asio::io_service::work work(service);
  std::thread thread([&service] { service.run(); });

  crawler::worker worker(service);
  try {
    auto opts = parse_command_line(argc, argv);
    configure_logger(opts);
    configure_worker(opts, worker);
    read_and_add_credentials(opts, worker);
    read_and_push_servers(opts, worker);

    worker.run();
    worker.block_until_complete();
    
    write_results(opts, worker);
  } catch (const std::exception &_exc) {
    std::cerr << _exc.what() << std::endl;
  }

  service.stop();
  thread.join();

  return 0;
}
