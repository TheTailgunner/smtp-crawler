#include "worker.h"
#include "smtp-client.h"
#include "easylogging++.h"

using namespace boost::asio;

namespace crawler {
  worker::worker(io_service &_serv,
		 std::size_t _n_threads): m_io_service{_serv},
					  m_thread_count{_n_threads},
					  m_tasks_pending{0},
					  m_need_stop{false},
					  m_mode{mode::to_end}
									 {}
  worker::~worker() { stop(); }

  void worker::run() {
    LOG(DEBUG) << "starting thread pool of " << m_thread_count <<  " workers";
    
    for (std::size_t i_thread = 0; i_thread < m_thread_count; ++i_thread) {
      m_threads.create_thread(boost::bind(&crawler::worker::do_work, this));
    }
  }

  void worker::block_until_complete() {
    boost::unique_lock<boost::mutex> guard(m_mutex);
    m_stop_cond.wait(guard, [this]() { return this->m_need_stop; });
    m_need_stop = false;
    
    LOG(DEBUG) << "worker terminate condition triggered - terminating";
  }

  void worker::stop() noexcept {
    m_threads.interrupt_all();
    LOG(DEBUG) << "thread pool interrupted";
  }

  void worker::add_login_pair(const std::string &_login,
			      const std::string &_pass) {
    m_logins.push_back(
		       std::make_pair(_login, _pass)
		       );
  }
  
  void worker::push_task(worker::task _task) {
    m_tasks.push(_task);
    ++m_tasks_pending;
  }

  void worker::process_server(smtp::smtp_client &_client,
			      worker::task _serv) {
    LOG(INFO) << "processing server " << _serv.ip << ":"
	       << _serv.port;
    
    for (auto &cred : m_logins) {
      LOG(INFO) << "\ttrying login " << cred.first << " password " << cred.second;
      
      try {
	if (!_client.try_connect(_serv.ip, _serv.port))
	  continue;
	
	if (_client.try_login(cred.first, cred.second)) {
	  LOG(INFO) << "\tlogin accepted by " << _serv.ip << ":" << _serv.port;
	  push_result(_serv, cred);

	  if (mode::stop_on_first == m_mode) {
	    m_need_stop = true;
	    m_stop_cond.notify_one();
	  }
	    
	} else {
	  LOG(INFO) << "\tlogin declined by " << _serv.ip << ":" << _serv.port;
	}
	
	_client.terminate();
	LOG(INFO) << "\t----";
      } catch (const std::runtime_error &exc) {
	LOG(ERROR) << exc.what();
      }
    }
  }

  void worker::push_result(worker::task _serv,
			   worker::login_pair _cred) {
    boost::lock_guard<boost::mutex> guard(m_mutex);

    m_results.push_back(
			std::make_pair(_serv, _cred)
			);
  }
  
  void worker::do_work() {    
    crawler::smtp::smtp_client client(m_io_service);
    client.set_connect_timeout(m_smtp_timeout);
    
    while (true) {
      boost::this_thread::interruption_point();
      
      if (m_tasks.empty())
	continue;
      task t;
      m_tasks.pop(t);

      boost::this_thread::interruption_point();
      
      process_server(client, t);

      if (m_tasks_pending)
	--m_tasks_pending;

      if (0 == m_tasks_pending) {
	m_need_stop = true;
	m_stop_cond.notify_one();
      }
      
      boost::this_thread::interruption_point();
    }
  }
}

