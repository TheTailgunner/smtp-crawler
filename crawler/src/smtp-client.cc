#include "smtp-client.h"

#include <algorithm>
#include <regex>

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/remove_whitespace.hpp>

#include "easylogging++.h"

namespace crawler {
  namespace smtp {
    smtp_client::smtp_client(boost::asio::io_service &_serv): m_connected(false),
      m_network(_serv) { }

    bool smtp_client::try_connect(const std::string &_addr,
				  std::size_t _port) noexcept {
      try {
	m_network.connect(_addr, _port);

	// flush server greeting string
	tcp_smtp_stream strm(m_network);
	smtp_response_list greeting;
	LOG(DEBUG) << "\t\treading server greeting string ";
	strm >> greeting;
	LOG(DEBUG) << "\t\tserver greeting " << greeting.at(0).text();
	
	m_connected = true;
      } catch (const boost::system::system_error &exc) {
	LOG(ERROR) << exc.what();
	return false;
      } catch (const network::timeouted_exception &_exc) {
	LOG(ERROR) << "connection timeout exceeds (" << m_network.timeout()
		   << " sec)";
	return false;
      }
      return true;
    }

    bool smtp_client::try_login(const std::string &_login,
				const std::string &_pass) {
      if (!m_connected)
	return false;
      
      smtp_response_list ehlo_resp = say_ehlo();

      LOG(DEBUG) << "\t\tdetecting STARTTLS";
      if (provides_tls(ehlo_resp)) {
	LOG(DEBUG) << "\t\tTLS provided";
	start_tls();
	ehlo_resp = say_ehlo();
      } else {
	LOG(DEBUG) << "\t\tno TLS provided - bypass directly to AUTH PLAIN";
      }

      // no matter TLS provided or not, bypass to AUTH PLAIN
      if (provides_auth_plain(ehlo_resp)) {
	LOG(DEBUG) << "\t\tAUTH PLAIN detected";
	return auth_plain(_login, _pass);
      }

      return false;
    }

    void smtp_client::terminate() noexcept {
      if (!m_connected)
	return;
      try {
	tcp_smtp_stream ss(m_network);
	ss << "QUIT";
      
	m_network.disconnect();
      } catch (...) { }
    }

    bool smtp_client::provides_tls(const smtp_response_list &_list) {
      return std::count_if(_list.cbegin(), _list.cend(), [](auto &resp) {
	  return resp.text() == "STARTTLS";
	}) > 0;
    }

    void smtp_client::start_tls() {
      tcp_smtp_stream ss(m_network);
      ss << "STARTTLS";

      smtp_response_list resp;
      ss >> resp;

      if (!resp.at(0).good())
	throw std::runtime_error("unable to STARTTLS");

      m_network.start_tls();
    }

    bool smtp_client::provides_auth_plain(const smtp_response_list &_resp) {
      return std::count_if(_resp.cbegin(), _resp.cend(),
			   [](auto &response) {
			     return std::regex_match(response.text(),
						     std::regex("AUTH.*PLAIN.*"));
			   }) > 0;
    }

    bool smtp_client::auth_plain(const std::string &_login,
				 const std::string &_pass) {
      
      std::string credentials;
      credentials.push_back('\0');
      credentials += _login;
      credentials.push_back('\0');
      credentials += _pass;
      std::string command = "AUTH PLAIN " + encode_base64(credentials);

      tcp_smtp_stream strm(m_network);
      strm << command;

      smtp_response_list resp;
      strm >> resp;

      return resp.at(0).good();
    }

    std::string smtp_client::encode_base64(const std::string &_data) {
      using namespace boost::archive::iterators;
      typedef insert_linebreaks<base64_from_binary<transform_width<std::string::const_iterator,6,8> >, 72 > it_base64_t;

      unsigned int pad_chars = (3 - _data.length() % 3) % 3;
      std::string base64(it_base64_t(_data.begin()),
		    it_base64_t(_data.end()));
      base64.append(pad_chars, '=');

      return base64;
    }
    
    smtp_client::smtp_response_list smtp_client::say_ehlo() {
      tcp_smtp_stream strm(m_network);
      strm << "EHLO localhost";

      smtp_response_list response;
      strm >> response;

      return response;
    }
    
    smtp_client::~smtp_client() {
      terminate();
    }
  }
}
