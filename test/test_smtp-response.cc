#include <smtp-response.h>
#include <gmock/gmock.h>
#include <stdexcept>

using namespace crawler::smtp;

TEST(SMTP_Response, CtrThrowsWhenResultCodeNotExactly_3_Digits) {
  EXPECT_NO_THROW(smtp_response("250", "TEST"));
  
  EXPECT_THROW(smtp_response("", "TEST"), std::runtime_error);
  EXPECT_THROW(smtp_response("2", "TEST"), std::runtime_error);
  EXPECT_THROW(smtp_response("3456", "TEST"), std::runtime_error);
}

TEST(SMTP_Response, CtrThrowsWithWrong_FirstDigit_OfCode) {
  EXPECT_THROW(smtp_response("134", "TEST"), std::runtime_error);
}

TEST(SMTP_Response, ReflectsTheResultOfRequest) {
  smtp_response PositiveCompletedResponse("2xx", "TEST");
  smtp_response PositiveIntermediateResponse("3xx", "TEST");
  smtp_response TemporaryNegativeResponse("4xx", "TEST");
  smtp_response NegativeResponse("5xx", "ERROR");

  ASSERT_EQ(PositiveCompletedResponse.status(),
	    smtp_response::response_status::positive_completed);
  ASSERT_EQ(PositiveIntermediateResponse.status(),
	    smtp_response::response_status::positive_intermediate);
  ASSERT_EQ(TemporaryNegativeResponse.status(),
	    smtp_response::response_status::negative_transient);
  ASSERT_EQ(NegativeResponse.status(),
	    smtp_response::response_status::negative_permanent);
  
}
