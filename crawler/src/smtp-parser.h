// -*-C++-*-
#pragma once

#include <cstdint>
#include <vector>

#include "smtp-response.h"

namespace crawler {
  namespace smtp {
    class smtp_parser {
    public:
      enum class parser_state {
	response_code, delimiter, response_message, terminator
	  };
      bool try_parse(const std::vector<std::uint8_t> &,
		     std::vector<smtp_response> &);
      parser_state state() const { return m_state; }
      void reset();

      // only for sake of testing
      const std::string &current_code_string() const { return m_current_code; }
      const std::string &current_text_string() const { return m_current_text; }
      bool awaits_multiline() const { return m_awaits_multiline; }
    private:
      parser_state m_state{parser_state::response_code};
      std::string m_current_code;
      std::string m_current_text;
      unsigned char m_last_sym{0};
      bool m_awaits_multiline{false};
      bool m_multiline_last{false};
    };
  }
}
