// -*-C++-*-
#pragma once

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <cstdint>
#include <vector>
#include <cstdlib>
#include <stdexcept>

namespace crawler {
  namespace network {
    using namespace boost::asio;

    class timeouted_exception: public std::exception {};
    
    class tcp_client: private boost::noncopyable {
    public:
      static const std::size_t DEFAULT_TIMEOUT_SEC{3};
      
      tcp_client(boost::asio::io_service &);
      ~tcp_client();
      tcp_client(tcp_client &&) = default;
      tcp_client &operator=(tcp_client &&) = default;

      void set_timeout(std::size_t _tm) { m_timeout = _tm; }
      std::size_t timeout() const { return m_timeout; }
      
      void start_tls();
      void connect(const std::string &,
		   std::uint16_t);
      void write(const std::vector<std::uint8_t> &);
      std::vector<std::uint8_t> read();
      
      void disconnect();
    private:
      bool m_ssl_enabled;
      std::size_t m_timeout{DEFAULT_TIMEOUT_SEC};
      ssl::context m_ssl_context;
      ssl::stream<ip::tcp::socket> m_socket;
      ip::tcp::resolver m_resolver;
    };
  }
}
